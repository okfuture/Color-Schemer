# Color-Schemer
Create nice palettes of color

Try the demo: https://xmatthewx.github.io/Color-Schemer/


### Credit

This project utilizes a library called color-scheme.js (based on a perl module) and borrows heavily from its demonstration. I aimed to simply the results, to create consistent four color palettes. I added a demonstration to help a user evaluate the colors in use.   

Check it out: https://github.com/c0bra/color-scheme-js/